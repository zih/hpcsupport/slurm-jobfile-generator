#!/usr/bin/python3

"""
    This file is part of sgen software.
    Slurm Jobfile Generator

    Copyright (c) 2022,
        Technische Universitaet Dresden, Germany

    sgen is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with sgen.  If not, see <http://www.gnu.org/licenses/>.
"""

import pytest
from conftest import selenium_run_test

# Acceptable workspace names include
#   Valid characters for workspace names are only alphanumeric characters, -, ., and _.


@pytest.mark.parametrize(
    "wsname, expected_result",
    [
        ({"ws-name": ""}, False),
        ({"ws-name": "foo"}, True),
        ({"ws-name": "foo-bar"}, True),
        ({"ws-name": "Foo.Bar"}, True),
        ({"ws-name": " foo "}, True),
        ({"ws-name": "bar42"}, True),
        ({"ws-name": "foo_bar"}, True),
        ({"ws-name": ".bar"}, False),
        ({"ws-name": "foo-bar.baz_foz"}, True),
        ({"ws-name": "foo=bar"}, False),
        ({"ws-name": "----"}, False),
        ({"ws-name": "foo/bar"}, False),
        ({"ws-name": "foo", "duration": "0"}, False),
        ({"ws-name": "foo", "duration": "2"}, True),
        ({"ws-name": "foo", "duration": "33"}, True),
        ({"ws-name": "foo", "duration": "33.5"}, False),
        ({"ws-name": "foo", "duration": "-7"}, False),
        ({"ws-name": "foo", "duration": "123"}, False),
        ({"workspace-filesystem": "archive", "ws-name": "foo", "duration": "123"}, True),
        ({"workspace-filesystem": "ssd", "ws-name": "foo", "duration": "30"}, True),
        ({"workspace-filesystem": "ssd", "ws-name": "foo", "duration": "31"}, False),
        ({"workspace-filesystem": "ssdfoo", "ws-name": "foo", "duration": "31"}, "NoSuchElementException"),
    ],
)
def test_is_valid_wsname(driver, wsname, expected_result):
    actions = dict({"check-workspace": True}, **wsname)
    selenium_run_test(driver, actions, expected_result)
