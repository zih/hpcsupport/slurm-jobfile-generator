import pytest
from conftest import is_valid_entry, selenium_run_test

"""
All tests will fail, because mail and mail type are required.
"""


@pytest.mark.parametrize(
    "mail, expected_result", [("", True), ("wen@tu-dresden.de", False), ("foo.bar", False), ("wen@tu-dresden", False)]
)
def test_is_validmail(driver, mail, expected_result):
    assert is_valid_entry(driver, "mail", mail) == expected_result


"""
Test combination of mail and mail type.
"""


@pytest.mark.parametrize(
    "mail_type, expected_result",
    [
        ({"mail-begin": True}, False),
        ({"mail-begin": True, "mail": "wen@tu-dresden.de"}, True),
        ({"mail-end": True}, False),
        ({"mail-end": True, "mail": "wen@tu-dresden.de"}, True),
        ({"mail-fail": True}, False),
        ({"mail-fail": True, "mail": "wen@tu-dresden.de"}, True),
    ],
)
def test_is_validmailsetting(driver, mail_type, expected_result):
    selenium_run_test(driver, mail_type, expected_result)
