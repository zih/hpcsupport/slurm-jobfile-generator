#!/usr/bin/python3

"""
    This file is part of sgen software.
    Slurm Jobfile Generator

    Copyright (c) 2022,
        Technische Universitaet Dresden, Germany

    sgen is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with sgen.  If not, see <http://www.gnu.org/licenses/>.
"""

import pytest
from conftest import is_valid_entry


@pytest.mark.parametrize(
    "array, expected_result",
    [
        ("", True),
        ("12", True),
        ("1-2", True),
        ("1,2,3,4", True),
        ("1-5:2", True),
        ("1-5:2,5", False),  # TODO test in slurm if this should work
        (" 12 ", True),
        ("12%2", True),
        ("1-2%2", True),
        ("1,2,3,4%2", True),
        ("1-5:2%2", True),
        (" 12%2 ", True),
        ("-", False),
        ("ab", False),
        ("11;", False),
        ("a-11", False),
        ("1,2,a,4", False),
        ("12%_", False),
        ("1--4", False),
    ],
)
def test_is_validtimelimit(driver, array, expected_result):
    assert is_valid_entry(driver, "array", array) == expected_result
