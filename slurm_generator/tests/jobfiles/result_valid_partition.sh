#!/bin/bash

#SBATCH --job-name="foo"
#SBATCH --account="bar"
#SBATCH --mail-user=wen@tu-dresden.de
#SBATCH --mail-type=FAIL
#SBATCH --time=1-02:03:04
#SBATCH --partition=gpu
#SBATCH --nodes=16
#SBATCH --ntasks=128
#SBATCH --gres=gpu:4

# Setup computational environment, i.e, load desired modules
# module purge
# module load <module name>



# Execute parallel application 
srun hostname