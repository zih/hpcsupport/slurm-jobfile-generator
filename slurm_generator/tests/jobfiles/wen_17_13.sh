#!/bin/bash

#SBATCH --job-name="foo"
#SBATCH --account="bar"
#SBATCH --mail-user=wen@tu-dresden.de
#SBATCH --mail-type=FAIL
#SBATCH --time=1-02:03:04
#SBATCH --partition=png
#SBATCH --nodes=13
#SBATCH --ntasks=17

# Setup computational environment, i.e, load desired modules
# module purge
# module load <module name>



# Execute parallel application 
srun hostname
