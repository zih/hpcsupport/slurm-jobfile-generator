# Testing

## Preparation

0. Prepare your environment. You might need to install the packages provided in the `tests/requirements.txt` file. It is perfectly fine to create a virtual environment.
1. Change to the directory `tests` and execute `sh prepare_tests.sh`. This will parse the provided configuration files and create a `slurm_generator.html` output file.

## Execution

Run `pytest` (or `../runtests.py`).


## Tests

### Unit-Tests

* `test_timelimit`: Tests for valid and invalid values for the time limit form.
* `test_mailform`: Tests for valid and invalid values for mail and mail type forms.
* `test_array`: Tests for valid and invalid values for the array form.
* `test_nnodes`: Tests for valid and invalid values for the nodes form

### 

* `test_download`: Fill forms and compares generated jobfile with expected one TODO: Is it really downloading anything? No, it only reads the stuff from page
* `test_complete_files`: Fill forms and compares generated jobfile with expected one