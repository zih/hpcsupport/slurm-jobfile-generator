"""
    This file is part of sgen software.
    Slurm Jobfile Generator

    Copyright (c) 2022,
        Technische Universitaet Dresden, Germany

    sgen is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with sgen.  If not, see <http://www.gnu.org/licenses/>.
"""

import pytest
from conftest import is_valid_entry

# Tests for form "Nodes (-N, --nodes)"


@pytest.mark.parametrize(
    "nnodes, expected_result",
    [("1", True), ("12", True), ("-1", False), ("many", False), ("12.2", False), ("0", False), ("0.1", False)],
)
def test_is_validnodesetting(driver, nnodes, expected_result):
    assert is_valid_entry(driver, "nodes", nnodes) == expected_result
