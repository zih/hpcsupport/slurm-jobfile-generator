#!/usr/bin/env python3

"""
    This file is part of sgen software.
    Slurm Jobfile Generator

    Copyright (c) 2022,
        Technische Universitaet Dresden, Germany

    sgen is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with sgen.  If not, see <http://www.gnu.org/licenses/>.
"""

import os

import pytest
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

script_dir = os.path.abspath(os.path.dirname(__file__))


@pytest.fixture(scope="module")
def driver():
    d = webdriver.Firefox()
    d.get("file://" + script_dir + "/slurm_generator.html")
    yield d
    d.close()


def selenium_run_test(driver, input_dict, expected_result, expected_output=None):
    driver.get("file://" + script_dir + "/slurm_generator.html")

    # Go through the input dict and enter all date in the specified fields
    for k, v in input_dict.items():
        elem = driver.find_element(By.ID, k)
        # Dropdown menus
        if k in ("byte", "partition", "type-depend", "workspace-filesystem"):  # TODO think about global list
            try:
                Select(elem).select_by_visible_text(v)
            except NoSuchElementException:
                assert expected_result == "NoSuchElementException"
                return
        # Checkboxes
        elif (
            k
            in (
                "mail-all",
                "mail-begin",
                "mail-end",
                "mail-fail",
                "nomultithread",
                "exclusive",
                "one-output",
                "check-workspace",
                "check-delete",
            )  # TODO think about global list
            and v
        ):
            elem.click()
        # Text fields
        else:
            elem.clear()
            elem.send_keys(v)
            elem.send_keys(Keys.RETURN)
    # Press generate Button
    elem = driver.find_element(By.ID, "generate-button")
    elem.send_keys(Keys.RETURN)

    # If expected_result is True, this field should not exist
    assert (
        '<label id="output-text" class="limits" style="display: block;">Output requires update</label>'
        in driver.page_source
    ) != expected_result, (
        "This configuration should " + ("" if not expected_result else "not ") + "have thrown an error!"
    )
    print("This jobfile threw the expected error")

    # If an output file is supplied for comparison
    if expected_output is not None:
        elem = driver.find_element(By.ID, "output")
        with open(expected_output, "r") as f:
            expected_jobfile = f.read().strip()
        assert elem.text == expected_jobfile, "Generated Slurm-jobfile does not correspond to expected output!"


def is_valid_entry(driver, element_id, value):
    elem = driver.find_element(By.ID, element_id)
    elem.clear()
    elem.send_keys(value)
    elem.send_keys(Keys.RETURN)

    elem = driver.find_element(By.ID, "generate-button")
    elem.send_keys(Keys.RETURN)

    if (
        '<label id="output-text" class="limits" style="display: block;">Output requires update</label>'
        in driver.page_source
    ):
        return False
    return True
