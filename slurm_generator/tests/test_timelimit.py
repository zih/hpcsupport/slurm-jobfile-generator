#!/usr/bin/python3

"""
    This file is part of sgen software.
    Slurm Jobfile Generator

    Copyright (c) 2022,
        Technische Universitaet Dresden, Germany

    sgen is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with sgen.  If not, see <http://www.gnu.org/licenses/>.
"""

import pytest
from conftest import is_valid_entry

# Acceptable time formats include
#   "minutes", "minutes:seconds", "hours:minutes:seconds",
#   "days-hours", "days-hours:minutes" and "days-hours:minutes:seconds".


@pytest.mark.parametrize(
    "timelimit, expected_result",
    [
        ("", True),
        ("12", True),
        ("1234567", True),
        ("11:12", True),
        ("1:2", True),
        (" 1:2 ", True),
        ("00:12", True),
        ("11:12:13", True),
        ("02-11:12:13", True),
        ("11-12:13", True),
        ("-", False),
        ("ab", False),
        ("11;12:13", False),
        ("a-11:12:13", False),
    ],
)
def test_is_validtimelimit(driver, timelimit, expected_result):
    assert is_valid_entry(driver, "timelimit", timelimit) == expected_result
