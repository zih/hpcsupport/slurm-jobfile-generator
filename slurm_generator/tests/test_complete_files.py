"""
    This file is part of sgen software.
    Slurm Jobfile Generator

    Copyright (c) 2022,
        Technische Universitaet Dresden, Germany

    sgen is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with sgen.  If not, see <http://www.gnu.org/licenses/>.
"""

from conftest import script_dir, selenium_run_test

STANDARD_DICT = {
    "job-name": "foo",
    "account": "bar",
    "mail": "wen@tu-dresden.de",
    "mail-fail": True,
    "timelimit": "1-02:03:04",
    "nodes": "13",
    "tasks": "17",
    "executable": "hostname",
}


def test_default_case(driver):
    # Test Input of minimum required fields
    expected_result = True
    expected_result_file = script_dir + "/jobfiles/wen_17_13.sh"
    selenium_run_test(driver, STANDARD_DICT, expected_result, expected_result_file)


def test_no_input(driver):
    expected_result = True
    selenium_run_test(driver, {}, expected_result)


def test_valid_partition(driver):
    input_dict = {**STANDARD_DICT, "partition": "gpu", "nodes": "16", "tasks": "128", "gpus": "4"}
    expected_result = True
    expected_result_file = script_dir + "/jobfiles/result_valid_partition.sh"
    selenium_run_test(driver, input_dict, expected_result, expected_result_file)
