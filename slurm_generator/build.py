#!/usr/bin/env python3

"""
    This file is part of sgen software.
    Slurm Jobfile Generator

    Copyright (c) 2022,
        Technische Universitaet Dresden, Germany

    sgen is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with sgen.  If not, see <http://www.gnu.org/licenses/>.
"""

import argparse
import json
import math
import os
import pathlib
import re
import sys

from hostlist import expand_hostlist

NEEDED_PARAMS = [
    "MaxTime",
    "DefaultTime",
    "Description",
    "Sockets",
    "CoresPerSocket",
    "ThreadsPerCore",
    "nodes",
    "GPU",
    "HTCores",
    "Cores",
    "MemoryPerNode",
    "MemoryPerCore",
]
NODE_PARAMS = ["Sockets", "CoresPerSocket", "ThreadsPerCore", "RealMemory", "Gres", "Procs"]


def format_json_for_js(partitions_dict, lead_spaces):
    """Format the partitions dict and write it to string that is a js hash table

    Args:
        partitions_dict <str>: The partitions dict

    Returns:
        <str>: The partitions_dict as a js hash table
    """
    # Make every parameter a number if possible
    for partition in partitions_dict:
        for key in partitions_dict[partition]:
            try:
                num = int(partitions_dict[partition][key])
                partitions_dict[partition][key] = num
            except:
                continue
    formatted_json = json.dumps(partitions_dict, indent=2)
    # Remove outer braces
    lines = formatted_json.split("\n")[1:-1]

    re_dict = r'  "([a-zA-Z0-9]+)": {'
    i = 0
    for line in lines:
        match = re.match(re_dict, line)
        if match:
            partition_name = match.group(1)
            lines[i] = f'  "{partition_name}" : {partition_name} = {{'
        i += 1
    formatted = " " * lead_spaces + f'\n{" " * lead_spaces}'.join(lines)
    return formatted.replace('"', "'")


def get_first_node(noderange, nodes_dict):
    """Takes a node range and a node dict and returns the node definition of the
    first matching node.

    Args:
        node range <str>: A Node range e.g. taurus[01-20]
        nodes_dict dict<,>: A dictionary of node ranges and their parameters

    Returns:
        dict: dict of the parameters of the first matched node definition
    """
    # Loop every node in the partition and look until we find a defined node
    for node in expand_hostlist(noderange):
        for def_nodes in nodes_dict:
            if node in nodes_dict[def_nodes]["NodeList"]:
                return nodes_dict[def_nodes]

    return {}


def parse_config_line(line):
    """Takes a line from a Slurm configuration and outputs the parameters in a dict, overwrites
    applied parameters with parameters in the overwrite comment #! ...=... !#

    Args:
        line <str>: Line of a Slurm config

    Returns:
        dict<str,str>: Dictionary with the parameters of the line
    """
    # Split the Slurm definition and overwrite comment
    if "#!" in line:
        orig_str, overw_str = map(str.strip, line.split("#!"))
        overw_str = overw_str.split("!#")[0]
        overw_dict = dict(map(str.strip, x.split("=")) for x in overw_str.split("|"))
    else:
        orig_str = line
        overw_dict = {}

    # Put the partition parameter in a dict
    orig_list = orig_str.split(" ")
    orig_dict = dict(x.split("=", 1) for x in orig_list if x)

    # If overwrite params are defined, write the min the partition dict
    if overw_dict:
        for key in overw_dict.keys():
            orig_dict[key] = overw_dict[key]

    return orig_dict, overw_dict


def get_part_dict(partition, node, html_in):
    # get the number of spaces
    with open(html_in, "r") as file_in:
        for line in file_in:
            match = re.match(r"^( *)// %&dict_part&%\n$", line)
            if match:
                tabs = len(match.group(1)) - 2
                if tabs < 0:
                    tabs = 0
                break
    # Read node.conf (or slurm.conf) for Nodes and exctract info
    nodes_dict = {}
    with open(node, "r") as file_in:
        re_part = r"^NodeName=([^ ]+) "
        for line in file_in:
            line = line.strip()
            # Ignore lines that aren't Node definitions
            if not re.match(re_part, line):
                continue

            node_def_dict, x = parse_config_line(line)
            node_def_dict["NodeList"] = expand_hostlist(node_def_dict["NodeName"])
            nodes_dict[node_def_dict["NodeName"]] = node_def_dict

    # Read partition.conf (or slurm.conf) for Partitions and extract info
    partitions_dict = {}
    with open(partition, "r") as file_in:
        re_part = r"^PartitionName=([^ ]+) "
        re_hidden = r".+#!.*Hidden=NO.*!#"
        for line in file_in:
            line = line.strip()
            # Ignore lines that aren't Partition definitions
            if not re.match(re_part, line):
                continue
            # Ignore a hidden partition if there is no overwrite to show it
            if "Hidden=YES" in line and not re.match(re_hidden, line):
                continue

            part_dict, overwrite_dict = parse_config_line(line)
            node_info = get_first_node(part_dict["Nodes"], nodes_dict)

            for node_par in NODE_PARAMS:
                val = node_info.get(node_par, "")
                part_dict[node_par] = val

            # If overwrite params are defined, write the min the partition dict again
            # This way overwrites for node params behind partition params dont get lost
            if overwrite_dict:
                for key in overwrite_dict.keys():
                    part_dict[key] = overwrite_dict[key]

            # Calculate necessary Parameters
            # Number of nodes in the partition
            num_nodes = len(expand_hostlist(part_dict["Nodes"]))
            part_dict["nodes"] = num_nodes

            # Gres can be gpu:1 or gpu:tesla:4 but not Gpu:1
            part_dict["GPU"] = 0
            if part_dict["Gres"]:
                gres_parts = part_dict["Gres"].split(",")
                for gres in gres_parts:
                    parts = gres.split(":")
                    # If the gres is cpu, add teh number to GPU
                    if parts[0] == "gpu":
                        part_dict["GPU"] += int(parts[-1])

            part_dict["HTCores"] = int(part_dict["Procs"])
            part_dict["Cores"] = int(int(part_dict["Procs"]) / int(part_dict["ThreadsPerCore"]))

            part_dict["MemoryPerNode"] = int(part_dict["RealMemory"])

            mem = math.floor(part_dict["MemoryPerNode"] / part_dict["HTCores"])
            part_dict["MemoryPerCore"] = mem

            part_name = part_dict["PartitionName"]
            # Remove all irrelevant parameters from the dict
            for key in list(part_dict.keys()):
                if key not in NEEDED_PARAMS:
                    part_dict.pop(key)

            partitions_dict[part_name] = part_dict

    # Format output dict
    return format_json_for_js(partitions_dict, tabs)


def get_ws_dict(ws, html_in):
    # get the number of spaces
    with open(html_in, "r") as file_in:
        for line in file_in:
            match = re.match(r"^( *)// %&dict_ws&%\n$", line)
            if match:
                tabs = match.group(1)
                break
    # create empty result
    output_dict = ""
    valid_ws = False
    # TODO redo proposal: Read the ws.conf as yaml file with yaml.safe_load()
    # Then walk the lines and get the ws names that should not be displayed #!Hidden=YES!#
    # Use the dict from the yaml load for reading info and exclude HIDDEN ws.
    with open(ws, "r") as file_in:
        for line in file_in:
            # guards for hidden lines
            if re.match(r".+#!.*Hidden=YES.*!#", line):
                continue
            match = re.match(r"^  ([^ :]+):", line)
            if match:
                # get values from workspace
                if output_dict != "":
                    output_dict += ",\n"
                output_dict += tabs + "'" + match.group(1) + "' : " + re.sub(r"-", r"_", match.group(1)) + " = {\n"

                match = re.match(r".+#!.*\|?Description=([^\|]+)\|?.*!#", line)
                if match:
                    descr = match.group(1).replace("'", "\\'")  # replace ' with a \', as ' is for js strings
                    output_dict += tabs + "  'info' : '" + descr + "',\n"
                else:
                    output_dict += tabs + "  'info' : '',\n"
                # only write other values, if valid workspace was found
                valid_ws = True
                continue

            # write other values
            raw = re.match(r"^ {4}duration: ([0-9]+)\n", line)
            ref = re.match(r".*#!duration=([0-9]+)!#", line)
            match = ref if ref else raw
            if match and valid_ws:
                output_dict += tabs + "  'duration' : " + match.group(1) + ",\n"
                continue
            raw = re.match(r"^ {4}maxextensions: ([0-9]+)\n", line)
            ref = re.match(r".*#!maxextensions=([0-9]+)!#", line)
            match = ref if ref else raw
            if match and valid_ws:
                output_dict += tabs + "  'extensions' : " + match.group(1) + "\n"
                output_dict += tabs + "}"
                valid_ws = False
                continue

    output_dict += "\n"

    return output_dict


def get_info_dict(info, html_in):
    # get the number of spaces
    with open(html_in, "r") as file_in:
        for line in file_in:
            match = re.match(r"^( *)// %&dict_info&%\n$", line)
            if match:
                tabs = match.group(1)
    with open(info, "r") as file_in:
        data = json.load(file_in)
    data = tabs + "const info = " + json.dumps(data, indent=2) + ";"
    data = data.replace("'", "\\'")
    data = data.replace('"', "'")
    data = data.replace("\n", "\n" + tabs)
    data += "\n"
    return data


def write_to_file(slurm, partition, node, ws, html_in, html_out, md_out, info):
    part = get_part_dict(partition, node, html_in)
    ws = get_ws_dict(ws, html_in)
    info = get_info_dict(info, html_in)
    # copy substituted code into empty string
    result = ""
    with open(html_in, "r") as file_in:
        for line in file_in:
            line = re.sub(r"^( *)// %&dict_part&%\n$", part, line)
            line = re.sub(r"^( *)// %&dict_ws&%\n$", ws, line)
            line = re.sub(r"^( *)// %&dict_info&%\n$", info, line)
            result += line

    # create md file string
    result_md = "# Slurm Job Generator\n\n"  # add header for pipeline
    result_md += result
    result_md = re.sub(r"<!DOCTYPE html>\n", "", result_md)
    result_md = re.sub(
        r'<link type="text/css" href="../misc/style.css" rel="stylesheet">',
        r'<!-- <link type="text/css" href="../misc/style.css" rel="stylesheet"> -->',
        result_md,
    )
    result_md = re.sub(
        r'<script src="jquery-3.6.0.min.js"> </script>',
        r'<!-- <script src="jquery-3.6.0.min.js"> </script> -->',
        result_md,
    )

    # write finale string into a file
    with open(html_out, "w") as file_out:
        file_out.write(result)
    with open(md_out, "w") as file_out:
        file_out.write(result_md)


class ValidateFileArgument(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        if not os.path.isfile(values):
            print(f"Error: Cannot find file '{values}'. Exit.")
            sys.exit(1)
        else:
            setattr(namespace, self.dest, values)


def main():
    # get arguments of file paths
    parser = argparse.ArgumentParser(description="Provide config file data to html website")
    parser.add_argument(
        "--slurm",
        dest="slurm_conf",
        type=pathlib.Path,
        default="tests/slurm.conf",
        action=ValidateFileArgument,
        help="path to the slurm.conf file",
    )
    parser.add_argument(
        "--partition",
        dest="part_conf",
        type=pathlib.Path,
        default="tests/partition.conf",
        action=ValidateFileArgument,
        help="path to the partition.conf file",
    )
    parser.add_argument(
        "--node",
        dest="node_conf",
        type=pathlib.Path,
        default="tests/node.conf",
        action=ValidateFileArgument,
        help="path to the node.conf file",
    )
    parser.add_argument(
        "--ws",
        dest="ws_conf",
        type=pathlib.Path,
        default="tests/ws.conf",
        action=ValidateFileArgument,
        help="path to the ws.conf file",
    )
    parser.add_argument(
        "--info",
        dest="info_file",
        type=pathlib.Path,
        default="info.json",
        action=ValidateFileArgument,
        help="path to the info.conf file",
    )
    parser.add_argument(
        "--html_in",
        dest="html_in_file",
        type=pathlib.Path,
        default="base.html",
        action=ValidateFileArgument,
        help="path to the html input file",
    )
    parser.add_argument(
        "--html_out",
        dest="html_out_file",
        type=pathlib.Path,
        default="slurm_generator.html",
        help="path to the html output file",
    )
    parser.add_argument(
        "--md_out",
        dest="md_out_file",
        type=pathlib.Path,
        default="slurm_generator.md",
        help="path to the md output file",
    )
    parser.add_argument(
        "--assume-no",
        dest="assumeno",
        action="store_true",
        default=False,
        help="do not overwrite existing output files by default",
    )

    args = parser.parse_args()

    if args.html_in_file == args.html_out_file:
        print("html input and output can not be the same file")
        print("Program aborted")
        exit()
    if args.assumeno and os.path.exists(args.html_out_file):
        print("Output html file already exists. Type 'yes' to overwrite the file. ")
        if not input() == "yes":
            print("Program aborted")
            exit()
    if args.assumeno and os.path.exists(args.md_out_file):
        print("Output md file already exists. Type 'yes' to overwrite the file. ")
        if not input() == "yes":
            print("Program aborted")
            exit()

    write_to_file(
        args.slurm_conf,
        args.part_conf,
        args.node_conf,
        args.ws_conf,
        args.html_in_file,
        args.html_out_file,
        args.md_out_file,
        args.info_file,
    )


if __name__ == "__main__":
    main()
