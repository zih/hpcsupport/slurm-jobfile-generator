# sgen - Slurm Jobfile Generator

A html with javascript project to generate slurm files.

## Description
The project provides a web user interface, in which users can input the values they want and a javascript then generats a specific slurm file which can be copied or downloaded.

## Visuals
Each row has a label at the left side, which also includes the slurm specific flags, the info icon, which will show information about the field if hovered over, and the input field. The input fields may also have tooltips displaying the limits for this specific field. If `Generate` was pressed an the limits for a field were exceeded the field will be highlighted in red and a info text, displaying the limits will appear behind the input filed. \
The `Resources` part also has a information box at the right side to display the limits for the selected partition. \
For a better overview the values are grouped in collapsible fields. Collapsed groups have no impact on the generated result, so filled fields will always generate output. \
At the bottom is the `Generate` button, which must be pressed to generate the result. The generated result is in a box at the button of the page.If the input fields exceed the limits no output will be produced and a warning will appear to update the output after correcting the highlighted fields. There are also two button at the end of the page to copy the result to the clipboard and to save the result in a .sh file.

## Configuration and Installation

1. Clone the git repository from GitLab.
    ```Bash
    $ git clone https://gitlab.hrz.tu-chemnitz.de/zih/hpcsupport/slurm-jobfile-generator.git
    $ cd slurm-jobfile-generator.git/slurm_generator
    ```
1. Execute `build.py` to create the HTML page providing the _Slurm Jobfile Generator_.
    ```Bash
    $ python3 build.py --partition partition.conf --node node.conf --slurm slurm.conf --ws ws.conf --info info.json
    ```

To build the needed file you need to execute the `build.py`. This file requires the config files for the workspaces and config files for the Slurm options.

For a **quick start** and for testing purposes, you can invoke the `build.py` without any argument. Than, the default configuration files will be used to build the _Slurm Jobfile Generator_.

The syntax for the Slurm files is given by Slurm, while the workspace config must be in yaml syntax. A new comment can be used to add a custom description for the partition and workspace and also overwrite the informations of the conf. For futher information about this read the next section. Currently it is unpractical to use the tool with heterogen partitions having nodes with different specs, as the tool only reads the values for the first node.\
During this step all infomations from the `info.json` will be integrated into the output file. So any change in this file will require a new execution of `build.py` to be usable. \
This file can be given to the program via options or must be named as the default values and place beside the program file. The program will put out to files, a .md file and a .html file, which are similar.

### Customization to Your HPC System

Copy the configuration files from your HPC system to the local clone of the repository. In particular, you can customize the _Slurm Jobfile Generator_ by providing the following configuration files:

* Slurm configuration files `slurm.conf`, `partition.conf`, and `node.conf`
* Workspace configuration file `ws.conf`
* Tool tip configuration file `info.json`

Furthermore, the layout of the generated HTML file can be customized using CSS. The shipped CSS file is optimized for the TU Dresden's [HPC compendium](doc.zih.tu-dresden.de).

### Overwrite comments
The values for each partition can also be overwritten with a comment. For the slurm configs the synthax as as follows: \
`#!Description=...|Hidden=...|MaxTime=...|DefaultTime=...|Sockets=...|CoresPerSocket=...|ThreadsPerCore=...|Nodes=...|Procs=...|RealMemory=...|Gres=gpu:...!#` \
Description is used to give some information for which cases this partition is useful. The desciprtion can contain anything but `|`. The Hidden parameter can be used to overwrite the slurm value, to show or hide the partition; options are: NO or YES. All other options can be used to overwrite the regular option with a new value and only accept integers. Not all options must be used, just include the need ones separated with `|`.\
For the workspace config a silimar synthax is available. Here you can place a `#!Description=...|Hidden=...!#` comment behind the workspace name. The possibilities are as mentioned above. The duration value can be overwriten with a `#!duration=...!#` comment behind the line. Analog you can use a `#!maxextensions=...!#` comment.

## Usage
Open the website and then fill in the attribute you want to have in the slurm header. If you dont know what a specific attribute does, hover over the `i` icon and read the tooltip to get futher informations. The limits for the values are provided as tooltips. If the values exceed the limits the field will be highlighted and the limits will be displayed behind the field. These field must be corrected before a new output can be generated. \
If all needed fields are filled in, press the `Generate` button and at the button of the page the generated result will be displayed. If any field has incorrect input no new output will be generated until all fields have correct input; a label will show if the generation was not successful. Here you can copy the result to the clipboard or directly save the putput in a `.sh` file.

## Caveats
### GPU
To read the available GPU ressources, the build.py looks for the 'Gres' parameter in the slurm conf on the Node definition (NodeName). From this parameter, only the resource with the case sensitive name "gpu" will be read. (Slurm Definition for Gres: "<name>[:<type>][:no_consume]:<number>[K|M|G]") \
If you have a different definition for your GPU's be advised that you have to change code.

## Support
The best way is to use the GitLab repository and use the issue tracker. It is also possible to contact the hpc support of the Tu Dresden.

## Roadmap
Most features to be included in the future will be tracked as issues in the repository.

## Contributing
The projects git repository is public and so everybody with a GitLab account can contribute to this project. This way it is also possible to create new issues for the project.

## Authors and Acknowledgment
Author: Daniel Manja, Michael Müller, Martin Schroschk

## License
GPL V3 license.
